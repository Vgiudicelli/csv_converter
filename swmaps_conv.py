########################################################################################################################
##  Convertisseur de fichier csv SWMAPS vers QGIS                                                                     ##
##                                                                                                                    ##
##  @created_date:      2022.07.13                                                                                    ##
##  @author:            Giudicelli Vincent                                                                            ##
##  @email:             vincent.giudicelli@ensg.eu                                                                    ##
##  @git:               https://gitlab.com/Vgiudicelli/csv_converter                                                  ##
########################################################################################################################

import pandas as pd
import os
from proj import add_projection

from kml_swmaps import convert_doc as kml_convert


def _convert_geometry_leve(geom):
    par1 = geom.find("(")
    par2 = geom.find(")", par1)
    if not 0 <= par1 <= par2:
        raise ValueError("Can't recognize this geometry")
    tpe = geom[:par1]
    values = geom[par1+1:par2].split(" ")
    if tpe == "POINT Z ":
        lon, lat, h = list(map(float, values))
        return {"longitude":lon,
                "latitude":lat,
                "h":h}
    else:
        raise NotImplementedError(f"Geometry <{tpe}> unrecognized")

def _convert_geom_track(geom):
    par1 = geom.find("(")
    par2 = geom.find(")", par1)
    if not 0 <= par1 <= par2:
        raise ValueError("Can't recognize this geometry")
    tpe = geom[:par1]
    values = geom[par1+1:par2]#.split(" ")
    if tpe == "LINESTRING Z":
        l_pts = tuple(map(
            lambda e: tuple(map(
                float,
                e.split(" "))),
            values.split("; ")))
        return l_pts
    else:
        raise NotImplementedError("Geometry <{tpe}> unrecognized")

def convert_leve(path_in, path_out, add_projs = ()):
    """
        Converti un levé de SWmaps

        :path_input:  (str) chemin d'accès au fichier d'entrée
        :path_output: (str) chemin d'accès au fichier à enregistrer
        :add_projs:   (list[str]) liste des projections à ajouter (ex : ["EPSG:2154"] pour Lambert93)
    """    
    content = ""
    path_temp = path_in + "_temp.txt"
    with open(path_in, "r") as file:
        for ln in file:
            if content == "":
                content += ln
                continue
            ln = ln[:-2] + "\n"
            par1 = ln.find("(")
            par2 = ln.find(")", par1)
            if 0 <= par1 <= par2:
                content += ln[:par1] + ln[par1:par2].replace(",",".") + ln[par2:]
            else:
                content += ln

    with open(path_temp, "w") as file:
        file.write(content)

    df = pd.read_csv(path_temp, sep = ",", index_col = "ID")
    os.remove(path_temp)

    for index in df.index:
        for key, val in _convert_geometry_leve(df.loc[index, "Geometry"]).items():
            df.loc[index, key] = val

    df_out = df[df.columns.difference(["Geometry"])]
    
    # ajout des projections
    for proj in add_projs:
        add_projection(df_out, proj)

    df_out.to_csv(path_out)

def convert_trace(path_in, path_out, add_projs = ()):
        """
        Converti une trace de SWmaps

        :path_input:  (str) chemin d'accès au fichier d'entrée
        :path_output: (str) chemin d'accès au fichier à enregistrer
        :add_projs:   (list[str]) liste des projections à ajouter (ex : ["EPSG:2154"] pour Lambert93)
    """
    content = ""
    path_temp = path_in + "_temp.txt"
    with open(path_in, "r") as file:
        for ln in file:
            if content == "":
                content += ln
                continue
            ln = ln[:-2] + "\n"
            par1 = ln.find("(")
            par2 = ln.find(")", par1)
            if 0 <= par1 <= par2:
                content += ln[:par1] + ln[par1:par2].replace(",",";") + ln[par2:]
            else:
                content += ln

    with open(path_temp, "w") as file:
        file.write(content)


    df = pd.read_csv(path_temp, sep = ",", index_col = "Name")
    os.remove(path_temp)

    df_out = pd.DataFrame()
    id_pt = 0
    for track_name in df.index:
        geom, descr = df.loc[track_name, ["Geometry", "Remarks"]]
        l_pts = _convert_geom_track(geom)
        for i_pt in range(len(l_pts)):
            lon, lat, h = l_pts[i_pt]
            df_out.loc[id_pt, "track_name"] = track_name
            df_out.loc[id_pt, "id"] = int(i_pt)
            df_out.loc[id_pt, "description"] = descr
            df_out.loc[id_pt, "longitude"] = lon
            df_out.loc[id_pt, "latitude"] = lat
            df_out.loc[id_pt, "h"] = h
            id_pt += 1

    df_out = df_out.astype({"id":int})

    # ajout des projections
    for proj in add_projs:
        add_projection(df_out, proj)

    df_out.to_csv(path_out, index = False)


conversion_type = {
    "levé":convert_leve,
    "trace":convert_trace,
    "kml":kml_convert,
    }

conversion_ext = {
    "levé": [("CSV files","*.csv"),("CSV files","*.csv")],
    "trace":[("CSV files","*.csv"),("CSV files","*.csv")],
    "kml":[("KML files","*.kml"),("Archive","*.zip")],
    }

if __name__ == "__main__":
    print("""
Make convertion from swmaps files to gqis compatible csv

convert_leve (input, output, projections = ())
convert_trace(input, output, projections = ())
""")

# CSV_converter

Projet python permettant la convertion de fichier de coordonnées (du format Oruxmaps ou SWmaps) en CSV directement exploitable sous QGIS.

Dans le cadre de nouveaux capteurs GNSS low-cost (GEOSTIX) - permettant par exemple de "remplacer" la puce GPS d'un smartphone sous androïde, et d'utiliser n'importe quelle application liée au positionnement avec un capteur de précision centimétrique, comme OruxMaps ou SwMaps - de nouveaux formats de données sont produits. L'objectif de ce projet est de pouvoir convertir ces données en fichier CSV exploitables directement sous QGIS, avec les champs des coordonnées géographiques (longitude, latitude, hauteur), les champs éventuellements rajoutés lors de la saisie, et des champs liés à une reprojection des points.

## Requierments

- pyproj (`pip install pyproj`) (optionnel : permet de faire les différentes projections)
- pandas (`pip install pandas`)
- numpy (`pip install numpy`)

## Utilisation

Executer le fichier `converter.py` pour utiliser le logiciel avec son interface graphique.
Executer séparément les fichiers `*_conv.py` pour utiliser en console les convertisseurs.

## Authors and acknowledgment

Ce projet a été réalisé dans le cadre d'un projet étudiant de test de ces nouveaux capteurs à l'ENSG, commandité par l'IGN

Auteur :
- Giudicelli Vincent (vincent.giudicelli@ensg.eu)

## Project status

Ce projet est actuellement en phase de tests (juillet 2022)

########################################################################################################################
##  Ajout de projections                                                                                              ##
##                                                                                                                    ##
##  @created_date:      2022.07.20                                                                                    ##
##  @author:            Giudicelli Vincent                                                                            ##
##  @email:             vincent.giudicelli@ensg.eu                                                                    ##
##  @git:               https://gitlab.com/Vgiudicelli/csv_converter                                                  ##
########################################################################################################################

try:
    import pyproj
except ModuleNotFoundError:
    pyproj = None



def add_projection(df, proj):
    """
        Ajoute les colonnes de coordonnées projetées dans un DataFrame contenant au moins les colonnes "latitude" et
        "longitude" (EPSG:4326)

        :return: None
    """
    chp_x = "x_" + proj.replace(":","_")
    chp_y = "y_" + proj.replace(":","_")
    proj_geo = pyproj.Proj("EPSG:4326")
    proj_dest = pyproj.Proj(proj)
    tr = pyproj.transformer.Transformer.from_proj(proj_geo, proj_dest)
    pts = zip(df.loc[(), "latitude"], df.loc[(), "longitude"])
    for i, (x,y) in zip(df.index, tr.itransform(pts)):
        df.loc[i, chp_x] = x
        df.loc[i, chp_y] = y

# En cas d'erreur de chargement de pyproj
if pyproj is None:
    add_projection = lambda df, proj:None

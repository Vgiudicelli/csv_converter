########################################################################################################################
##  interface de conversion oruxmap et swmaps vers QGIS                                                               ##
##                                                                                                                    ##
##  @created_date:      2022.07.13                                                                                    ##
##  @last_modification: 2022.07.15                                                                                    ##
##  @author:            Giudicelli Vincent                                                                            ##
##  @email:             vincent.giudicelli@ensg.eu                                                                    ##
##  @git:               https://gitlab.com/Vgiudicelli/csv_converter                                                  ##
########################################################################################################################

import tkinter as tk
from tkinter import filedialog, ttk, messagebox
import os
try:
    import pyproj
except ModuleNotFoundError:
    pyproj = None
import json

from swmaps_conv import conversion_type as sw_conv,  conversion_ext as sw_ext
from orux_conv   import conversion_type as orx_conv, conversion_ext as orx_ext

applications = {
    "Oruxmaps" : orx_conv,
    "Swmaps"   : sw_conv,
    }

extensions = {
    "Oruxmaps" : orx_ext,
    "Swmaps"   : sw_ext,
    }

# Gestion des paramètres -----------------------------------------------------------------------------------------------
parameters_file = "param.json"
def load_parameters():
    global parameters
    parameters = dict(projections_default = {"EPSG:2154"},
                      path_default = "/",
                      app_default = "Oruxmaps",
                      type_default = "trace",
                      )
    if os.path.isfile(parameters_file):
        with open(parameters_file, "r") as file:
            parameters.update(json.load(file))
        if not os.path.isdir(parameters["path_default"]):
            parameters["path_default"] = "/"
        if not parameters["app_default"] in applications:
            parameters["app_default"] = list(applications)[0]
        if not parameters["type_default"] in applications[parameters["app_default"]]:
            parameters["type_default"] = list(applications[parameters["app_default"]])[0]
    else:
        save_parameters()
def save_parameters():
    with open(parameters_file, "w") as file:
        json.dump(parameters, file)
load_parameters()

# Utilitaires ----------------------------------------------------------------------------------------------------------
class Grille:
    """
        Utilitaire facilitant le placement des widgets dans la fenetre
    """
    def __init__(self, lines, columns, marge = 0.01):
        self.lines = lines
        self.columns = columns
        self.marge = marge

    def place_widget(self, widget, line, column, w = 1, h = 1):
        widget.place(rely = self.lines[line] + self.marge /2, relx = self.columns[column] + self.marge /2,
                     relheight = self.lines[line + h]     - self.lines[line]     - self.marge,
                     relwidth  = self.columns[column + w] - self.columns[column] - self.marge)
        
# Interface ------------------------------------------------------------------------------------------------------------
class ConverterApp:
    """
        Application (interface complète)
    """
    def __init__(self):
        self.fen = tk.Tk()
        self.fen.title("CSV_converter")
        self.fen.geometry("700x500")

        grille = Grille(columns = [0, .3, .8, 1], lines = {i:(i-1)/8 for i in range(10)})

        # Sélection de l'application qui a fourni le fichier 
        lbl_app = tk.Label(self.fen, text = "Application :", anchor = "e")
        self.selected_app = tk.StringVar(self.fen, parameters["app_default"])
        self.opt_app = tk.OptionMenu(self.fen, self.selected_app, *applications.keys(), command = self.update_type)
        grille.place_widget(lbl_app     , 1, 0)
        grille.place_widget(self.opt_app, 1, 1, w=2)

        # Sélection du format du fichier
        lbl_type = tk.Label(self.fen, text = "File type :", anchor = "e")
        self.selected_type = tk.StringVar(self.fen, "")
        self.opt_type = tk.OptionMenu(self.fen, self.selected_type, "")
        self.update_type()
        self.selected_type.set(parameters["type_default"])
        grille.place_widget(lbl_type     , 2, 0)
        grille.place_widget(self.opt_type, 2, 1, w=2)

        # Sélection du fichier à convertir
        lbl = tk.Label(self.fen, text = "input :", anchor = "e")
        self.input_path = tk.StringVar(self.fen, "")
        entry = tk.Entry(self.fen, textvariable = self.input_path)
        button = tk.Button(self.fen, text = "search", command = self.brows_input_file)
        grille.place_widget(lbl   , 3, 0)
        grille.place_widget(entry , 3, 1)
        grille.place_widget(button, 3, 2)

        # Sélection du fichier en sortie
        lbl = tk.Label(self.fen, text = "output :", anchor = "e")
        self.output_path = tk.StringVar(self.fen, "")
        entry = tk.Entry(self.fen, textvariable = self.output_path)
        button = tk.Button(self.fen, text = "search", command = self.brows_output_file)
        grille.place_widget(lbl   , 4, 0)
        grille.place_widget(entry , 4, 1)
        grille.place_widget(button, 4, 2)

        # Sélection des projections
        self.tree_proj = ttk.Treeview(self.fen, columns = ["projection"], show = "headings")
        self.tree_proj.bind("<Button>", lambda e:self.tree_proj.after(1,self.update_btn_remove_proj))
        self.tree_proj.heading("projection", text = "Projections")
        for proj in parameters["projections_default"]:
            self.tree_proj.insert("", 0, proj, value = (proj,))
        self.tree_proj.column(0, anchor = "center")
        btn_add = tk.Button(self.fen, text = "Add projection", command = self.add_proj)
        self.btn_remove = tk.Button(self.fen, text = "Remove projection", command = self.remove_proj)
        self.update_btn_remove_proj()
        if pyproj is None:
            self.tree_proj.delete(self.tree_proj.get_children(""))
            self.fen.after(100, lambda:self.show_error("Fail to import pyproj : any projection can be realized"))
            lbl_proj = tk.Label(self.fen, text = "Can't import module pyproj.")
            grille.place_widget(lbl_proj, 5, 0, h = 2, w = 3)
        else:
            grille.place_widget(btn_add,         5, 0)
            grille.place_widget(self.btn_remove, 6, 0)
            grille.place_widget(self.tree_proj,  5, 1, h=2, w=2)

        # Bouton de validation
        self.btn_convertir = tk.Button(self.fen, text = "CONVERT", command = self.clic_convert)
        grille.place_widget(self.btn_convertir, 7, 0, w = 3)

        btn_save = tk.Button(self.fen, text = "Save config", command = self.save_config)
        grille.place_widget(btn_save, 8, 0)
        btn_quit = tk.Button(self.fen, text = "Quit", command = self.fen.quit)
        grille.place_widget(btn_quit, 8, 1, w = 2)
        
    def save_config(self, event =  None):
        " enregistre la configuration (choix des projection, de l'application et du dossier choisi)"
        parameters["projections_default"] = self.tree_proj.get_children("")
        parameters["app_default"] = self.selected_app.get()
        parameters["type_default"] = self.selected_type.get()
        parameters["path_default"] = self.get_dir_paths()[0]
        save_parameters()
        self.show_info("Configuration saved")
    
    # Gestion des projections ------------------------------------------------------------------------------------------
    def add_proj(self, event = None):
        if pyproj is None:
            return
        fen = tk.Toplevel(self.fen)
        fen.title("Chose a projection")
        proj = tk.StringVar(fen, "")
        entry = tk.Entry(fen, textvariable = proj)
        entry.place(relx = .1, rely = .1, relwidth = .8, relheight = .4)
        valide = tk.Button(fen, text = "Add projection", command = fen.quit)
        valide.place(relx = .1, rely = .5, relwidth = .8, relheight = .4)
        fen.mainloop()
        prj = proj.get()
        if not prj:
            return
        try:
            fen.destroy()
        except tk._tkinter.TclError:
            return
        try:
            pyproj.Proj(prj)
        except pyproj.exceptions.CRSError:
            self.show_error(f"Unrecognized projection <{prj}>")
        else:
            if not prj in self.get_projection():
                self.tree_proj.insert("", 0, prj, value = (prj,))

    def update_btn_remove_proj(self, event = None):
        self.tree_proj.update()
        self.btn_remove.config(state = "disabled" if len(self.tree_proj.selection()) == 0 else "normal")

    def remove_proj(self, event = None):
        self.tree_proj.delete(self.tree_proj.selection())
        self.update_btn_remove_proj()

    def get_projection(self):
        return self.tree_proj.get_children("")
        
    # Gestion des erreurs ----------------------------------------------------------------------------------------------
    def show_error(self, error):
        if isinstance(error, Exception):
            text = f"({error.__class__.__name__}) {error}" 
        else:
            text = str(error)
        messagebox.showerror(message = text)

    def show_info(self, info):
        messagebox.showinfo(message = info)
            
    # Gestion des fichiers ---------------------------------------------------------------------------------------------
    def brows_input_file(self, event = None):
        input_path, output_path = self.get_dir_paths()
        rep = filedialog.askopenfilename(
            initialdir = input_path if input_path else output_path if output_path else parameters["path_default"],
            filetypes=(extensions[self.selected_app.get()][self.selected_type.get()][0],
                       ("all files", "*.*")))
        if rep: self.input_path.set(rep)

    def brows_output_file(self, event = None):
        input_path, output_path = self.get_dir_paths()
        rep = filedialog.asksaveasfilename(
            initialdir = output_path if output_path else input_path if input_path else parameters["path_default"],
            filetypes=(extensions[self.selected_app.get()][self.selected_type.get()][1],
                       ("all files", "*.*")))
        if rep: self.output_path.set(rep)

    def get_paths(self):
        output_path = self.output_path.get()
        input_path = self.input_path.get()
        if not os.path.isdir("/".join(output_path.split("/")[:-1])):  output_path = None
        if not os.path.isfile(input_path):   input_path  = None
        return (input_path, output_path)
        
    def get_dir_paths(self):
        output_path = "/".join(self.output_path.get().split("/")[:-1])
        input_path = "/".join(self.input_path.get().split("/")[:-1])
        if not os.path.isdir(output_path):  output_path = ""
        if not os.path.isdir(input_path):   input_path  = ""
        return (input_path, output_path)

    # Gestion des menus (selection app / selection type) ---------------------------------------------------------------
    def update_type(self, event = None):
        menu = self.opt_type["menu"]
        app = self.selected_app.get()
        types = applications[app]
        menu.delete(0, "end")
        for t in types:
            menu.add_command(label=t,
                             command=lambda value=t: self.selected_type.set(value))
        self.selected_type.set(list(types)[0])

    # Gestion de la conversion -----------------------------------------------------------------------------------------
    def clic_convert(self, event = None):
        self.btn_convertir.config(state = "disabled", text = "conversion...")
        self.btn_convertir.update()
        try:
            self.convert()
        finally:
            self.btn_convertir.config(state = "normal", text = "CONVERT")

    def convert(self):
        path_input, path_output = self.get_paths()
        if path_input is None:
            self.show_error("Invalid input file")
            return
        if path_output is None:
            self.show_error("Invalid output file")
            return
        function = applications[self.selected_app.get()][self.selected_type.get()]
        projections = self.tree_proj.get_children("")
        try:
            function(path_input, path_output, add_projs = projections)
        except Exception as e:
            self.show_error(f"conversion fail. Cause ({e.__class__.__name__}) : {e}")
        else:
            self.show_info("conversion done")

    # Gestion de la fenetre --------------------------------------------------------------------------------------------
    def mainloop(self):
        self.fen.mainloop()

    def destroy(self):
        try:
            self.fen.destroy()
        except tk._tkinter.TclError:
            pass

if __name__ == "__main__":
    app = ConverterApp()
    try:
        app.mainloop()
    finally:
        app.destroy()

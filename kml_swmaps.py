# transform KML

from bs4 import BeautifulSoup
import pandas as pd
import os, shutil
try:
    import pyproj
except ModuleNotFoundError:
    pyproj = None

def add_projection(df, proj):
    if pyproj is None: return
    chp_x = "x_" + proj.replace(":","_")
    chp_y = "y_" + proj.replace(":","_")
    proj_geo = pyproj.Proj("EPSG:4326")
    proj_dest = pyproj.Proj(proj)
    tr = pyproj.transformer.Transformer.from_proj(proj_geo, proj_dest)
    pts = zip(df.loc[(), "latitude"], df.loc[(), "longitude"])
    pts_lam = zip(df.index, tr.itransform(pts))
    for i, (x,y) in pts_lam:
        df.loc[i, chp_x] = x
        df.loc[i, chp_y] = y


def make_tree(a, indent = "\n"):
    for e in a:
        if e.name is None:
            print(" : [" + e, end = "]")
        else:
            print(indent + e.name, end = "")
            if len(e) != 0:
                make_tree(e, indent + "    ")

def get_balises(soup, name, profondeur = True):
    l = []
    for e in soup:
        if e.name == name:
            l.append(e)
        elif e.name is not None and profondeur:
            l += get_balises(e, name)
    return l


def get_layers(soup):
    return get_balises(soup, "folder")


class ContentError(Exception):
    pass

def get_value(element, key, index = 0):
    values = get_balises(element, key, False)
    if len(values) != 1:
        raise ContentError(f"{key} not found")
    try:
        value = values[0].contents[index]
    except (AttributeError, TypeError, IndexError):
        raise ContentError(f"Unvalid {key}")
    return value
    
def analyse_point(point):
    data = {}
#    data["name"] = get_value(point, "name")
    
    desc = get_value(point, "description", 1)
    soup = BeautifulSoup(desc, "lxml")
    body = get_balises(soup, "body")[0]
    try:
        layer, feature_id, remarks, fix_quality = body.contents[:4]
        data["layer"]       = layer      .contents[0][ 7:]
        data["feature_id"]  = feature_id .contents[0][12:]
        data["remarks"]     = remarks    .contents[0][ 9:]
        data["fix_quality"] = fix_quality.contents[0][13:]
    except (ValueError, AttributeError, TypeError, IndexError):
        raise ContentError("Fail to read content of a point")

    if len(body) == 5:
        table = body.contents[4]
        attributes = get_balises(table, "tr")
        for attribute in attributes:
            key, val = get_balises(attribute, "td")
            data[key.contents[0]] = val.contents[0]

    coords = get_value(point, "point").contents[0]
    lon, lat, h = map(float, coords.split(","))
    data["latitude"] = lat
    data["longitude"] = lon
    data["height"] = h
    return data


def analyse_layer_feature(layer):
    name = get_value(layer, "name")
    df = pd.DataFrame()
    points = get_balises(layer, "placemark", True)
    if len(points) == 0:
        raise ContentError("Any point found")
    for iPt in range(len(points)):
        point = analyse_point(points[iPt])
        df.loc[iPt, list(point.keys())] = list(point.values())
    return name, df


def analyse_linestring(line):
    name = get_value(line, "name")
    try:
        line = get_balises(line, "linestring")[0]
    except IndexError:
        raise ContentError("Not a linestring")
    coords = get_value(line, "coordinates")
    coords = [{k:float(v)
               for k,v in zip(("longitude", "latitude", "height"), pt.split(","))}
              for pt in coords.split(" ")]
    return name, coords

def analyse_layer_line(layer):
    name = get_value(layer, "name")
    df = pd.DataFrame()
    lines = get_balises(layer, "placemark", True)
    if len(lines) == 0:
        raise ContentError("Any point found")
    iPt = 0
    for line in lines:
        line_name, coords = analyse_linestring(line)
        for coord in coords:
            df.loc[iPt, "name"] = line_name
            df.loc[iPt, coord.keys()] = coord.values()
            iPt+=1
    return name, df

def convert_doc(path_input, path_output, add_projs = None):
    if path_input[-4:] != ".kml":
        raise ValueError("Invalid file format")
    with open(path_input, "r") as file:
        doc = "".join(file.readlines())
    soup = BeautifulSoup(doc.replace("\n",""), "html.parser")
    os.makedirs(path_output, exist_ok = True)
    try:
        file_name = get_balises(soup, "name")[0].contents[0]
        for layer in get_layers(soup):
            try:
                name, df = analyse_layer_feature(layer)
            except ContentError:
                try:
                    name, df = analyse_layer_line(layer)
                except ContentError:
                    raise NotImplementedError("this layer can't be resolved")
            # ajout des projections
            for proj in add_projs:
                add_projection(df, proj)
            df.to_csv(path_output + "/" + file_name + "_" + name + ".csv", index = False)
    finally:
        shutil.make_archive(path_output, "zip", path_output)
        shutil.rmtree(path_output)

#convert_doc("./mission12.3.kml", "./result", ["EPSG:2154"])

from converter import ConverterApp

###  mise à jour du chemin d'accès pour creation d'executables  ####################################
import os, sys
if getattr(sys, "frozen", False):
    os.chdir("/".join(sys.executable.split("/")[:-1]))

###  ajout d'un fichier d'erreurs  #################################################################
from datetime import datetime
def _write_error_(s, w = sys.stderr.write):
    w(s)
    n = datetime.now()
    date = "%2d:%2d:%2d.%6d    " % (n.hour, n.minute, n.second, n.microsecond)
    with open("errors.txt", "a") as f:
        s2 = s.replace("\n","\n"+len(date) * " ")
        f.write(f"{date}{s2}\n")
sys.stderr.write = _write_error_
with open("errors.txt","w"):pass   # efface le fichier
_write_error_("run\n", w = lambda s:())

###  métadonnées  ##################################################################################
with open("meta.txt", "w") as file:
    file.write("""Project version : 1.0.0
Date of starting version : 2022-05-25

if you find an error, please contact me at vincent.giudicelli@free.fr or vincent.giudicelli@ensg.eu""")


if __name__ == "__main__":
    try:
        app = ConverterApp()
        app.mainloop()
    finally:
        app.destroy()
    _write_error_("end")


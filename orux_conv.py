########################################################################################################################
##  Convertisseur de fichier csv Oruxmaps vers QGIS                                                                   ##
##                                                                                                                    ##
##  @created_date:      2022.07.13                                                                                    ##
##  @author:            Giudicelli Vincent                                                                            ##
##  @email:             vincent.giudicelli@ensg.eu                                                                    ##
##  @git:               https://gitlab.com/Vgiudicelli/csv_converter                                                  ##
########################################################################################################################

import pandas as pd
import os
from proj import add_projection

def convert_trace(path_input, path_output, add_projs = None):
    """
        Converti une trace de Oruxmaps

        :path_input:  (str) chemin d'accès au fichier d'entrée
        :path_output: (str) chemin d'accès au fichier à enregistrer
        :add_projs:   (list[str]) liste des projections à ajouter (ex : ["EPSG:2154"] pour Lambert93)
    """
    debug(f"reading file <{path_input}>...", end = "\t")
    df = pd.read_csv(path_input)
    debug("done")

    columns_output = {
        "Value 2":"longitude",
        "Value 1":"latitude",
        "Value 3":"h",
        "Value 4":"timestamp",
        }

    # extraction des lignes et colonnes
    debug("extracting coordinates...", end = "\t")
    extracted = df.loc[(df["Type"] == "Data") & (df["Message"] == "record")][columns_output.keys()].rename(columns = columns_output)
    extracted = extracted.astype({"timestamp":int})
    extracted["h"] = np.round(extracted["h"], decimals = 10)
    debug("done")

    # conversion de semicircles en degrés décimaux
    debug("converting from semicircle to degres...", end = "\t")
    extracted["latitude"] *= (180 / 2**31)
    extracted["longitude"] *= (180 / 2**31)
    debug("done")

    # ajout des projections
    for proj in add_projs:
        add_projection(extracted, proj)
    
    # enregistrement
    debug(f"saving file <{path_output}>...", end = "\t")
    extracted.to_csv(path_output, index = False)
    debug("done\n")


conversion_type = {
    "trace":convert_trace,
    }

conversion_ext = {
    "trace": [("CSV files","*.csv"),("CSV files","*.csv")],
    }

if __name__ == "__main__":
    print("""
Make convertion from oruxmaps files to gqis compatible csv

convert_trace(input, output, projections = ())
""")
